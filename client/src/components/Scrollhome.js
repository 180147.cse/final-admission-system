import React, { useEffect } from 'react';

function Scrollhome() {
    useEffect(() => {
        window.scrollTo(0, 0);
      }, []);
    
      return null;
}

export default Scrollhome