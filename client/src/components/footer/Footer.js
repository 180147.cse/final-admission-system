import React from "react";
import "./Footer.css";

const Footer = () => {
  return (
    <div className="footer">
      
      <p className="footerSection">
        © Powered by Department of CSE | JUST <br></br> Developer - Chayan Das
      </p>

    </div>
  );
};

export default Footer;
