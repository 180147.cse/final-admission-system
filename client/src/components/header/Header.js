import React from "react";
import "./Header.css";

function Header() {
  return (
    <div class="mainpageheader">
      <header>
        <img src="JUSTLogo.png" alt="just_logo" class="logo"></img>

        <p className="appname"> JUST Undergradute Final Admission</p>

      </header>
    </div>
  );
}

export default Header;
