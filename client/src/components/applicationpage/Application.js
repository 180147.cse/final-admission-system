import { useState } from "react";
import "./Application.css";
import "./Application1.css";


const Application = () => {

  const [currentStep, setCurrentStep] = useState(1);

  const handleNextStep = () => {
    setCurrentStep((prevStep) => prevStep + 1);
  };

  const handleNextStep1 = () => {
    setCurrentStep((prevStep) => prevStep + 2);
  };
  const handleNextStep2 = () => {
    setCurrentStep((prevStep) => prevStep + 3);
  };

  const handleNextStep3 = () => {
    setCurrentStep((prevStep) => prevStep + 4);
  };

  const handlePrevStep = () => {
    setCurrentStep((prevStep) => prevStep - 1);
  };

  const handlePrevStep1 = () => {
    setCurrentStep((prevStep) => prevStep - 2);
  };

  const handlePrevStep2 = () => {
    setCurrentStep((prevStep) => prevStep - 3);
  };

  const handlePrevStep3 = () => {
    setCurrentStep((prevStep) => prevStep - 4);
  };


  return (
    
    <div className="form_title">
      <div className="container">

        <div className="left-side">
         
         <div class="displayStep" id="displayStep">
           <div class="circle">

            
             <div class="circle1" id="circle1" style={{backgroundColor: currentStep
             ===1 ? "white" : "rgba(0, 0, 0, 0)" , color: currentStep
             ===1 ? "black" : "white"}} onClick={currentStep===2 ? handlePrevStep: currentStep===3? 
              handlePrevStep1 :currentStep===4? handlePrevStep2:currentStep===5? 
              handlePrevStep3:null}><p>1</p></div>

             <div class="circle1" id="circle2" style={{backgroundColor: currentStep
             ===2 ? "white" : "rgba(0, 0, 0, 0)" , color: currentStep
             ===2 ? "black" : "white"}} onClick={currentStep===1?handleNextStep:currentStep===3?handlePrevStep:currentStep===4?handlePrevStep1:currentStep===5?handlePrevStep2:null}><p>2</p></div>

             <div class="circle1" id="circle3" style={{backgroundColor: currentStep
             ===3 ? "white" : "rgba(0, 0, 0, 0)" , color: currentStep
             ===3 ? "black" : "white"}} onClick={currentStep===1?handleNextStep1:currentStep===2?handleNextStep: currentStep===4?handlePrevStep:currentStep===5?handlePrevStep1:null}><p>3</p></div>

             <div class="circle1" id="circle4" style={{backgroundColor: currentStep
             ===4 ? "white" : "rgba(0, 0, 0, 0)" , color: currentStep
             ===4 ? "black" : "white"}} onClick={currentStep===1?handleNextStep2:currentStep===2?handleNextStep1:currentStep===3?handleNextStep:currentStep===5?handlePrevStep:null}><p>4</p></div>

             <div class="circle1" id="circle5" style={{backgroundColor: currentStep
             ===5 ? "white" : "rgba(0, 0, 0, 0)" , color: currentStep
             ===5 ? "black" : "white"}} onClick={currentStep===1?handleNextStep3:currentStep===2?handleNextStep2:currentStep===3?handleNextStep1:currentStep===4?handleNextStep:null}><p>5</p></div>
           </div>
           
           <div class="steps">
             <div class="step1" onClick={currentStep===2 ? handlePrevStep: currentStep===3? 
              handlePrevStep1 :currentStep===4? handlePrevStep2:currentStep===5? 
              handlePrevStep3:null}>
               <span>Personal Information</span>
             </div>

             <div class="step1" onClick={currentStep===1?handleNextStep:currentStep===3?handlePrevStep:currentStep===4?handlePrevStep1:currentStep===5?handlePrevStep2:null}>
               <span>Addresses</span>
             </div>

             <div class="step1" onClick={currentStep===1?handleNextStep1:currentStep===2?handleNextStep: currentStep===4?handlePrevStep:currentStep===5?handlePrevStep1:null}>
               <span>Education</span>
             </div>

             <div class="step1" onClick={currentStep===1?handleNextStep2:currentStep===2?handleNextStep1:currentStep===3?handleNextStep:currentStep===5?handlePrevStep:null}>
               <span>Others</span>
             </div>

             <div class="step1" onClick={currentStep===1?handleNextStep3:currentStep===2?handleNextStep2:currentStep===3?handleNextStep1:currentStep===4?handleNextStep:null}>
               <span>Guardian</span>
             </div>
           </div>
         </div>

        </div>


        <div className="right-side">
         
          <div className="stepContainer" id="stepContainer">
            <div className="affichStep">

             {currentStep === 1 && (
              <div className="step-one" id="step-one">
                
               <p className="para1">Personal Information ( ব্যক্তিগত তথ্য )</p>

             <div className="form">
               <form>
                 <p className="label">শিক্ষার্থীর নাম <b>/</b> Student's name</p>
                 <input type="text" 
                 id="sname_eng" 
                 name="" 
                 placeholder="Student's name in english"/>

              
                 <input type="text" 
                 id="sname_bng" 
                 name="" 
                 lang="bn" data-avro
                 placeholder="Student's name in bengali (বাংলায়)"/>

                 <p className="label1">পিতার নাম <b>/</b> Father's name</p>
                 <input type="text" 
                 id="fname" 
                 name="" 
                 placeholder="Father's name......."/>
              

                 <p className="label1">মাতার নাম <b>/</b> Mother's name</p>

                 <input type="text" 
                 id="mname" 
                 name="" 
                 placeholder="Mother's name......."/>


                 <p className="label1">জন্ম তারিখ  (এসএসসি / সমমান   সনদ অনুযায়ী ) <b>/</b> Date of birth as per SSC or equivalent certificate</p>
                 <input type="date" 
                 id="sdob" 
                 name=""
                 />


                 <p className="label1">লিঙ্গ <b>/</b> Gender</p>
                 <select id="gender">
                 <option value="male">Male</option>
                 <option value="female">Female</option>
                 <option value="transgender">Transgender</option>
                 </select>
              

                 <p className="label1">বৈবাহিক অবস্থা <b>/</b> Marital status</p>

                 <select id="marital_status">
                 <option value="maried">Married</option>
                 <option value="unmaried">Unmarried</option>
                 </select>


                 <p className="label1">ধর্ম  <b>/</b> Religion</p>

                 <input type="text" 
                 id="religion" 
                 name="" 
                 placeholder="Religion......."/>


                 <p className="label1">সম্প্রদায়  <b>/</b> Caste (optional..)</p>

                 <input type="text" 
                 id="caste" 
                 name="" 
                 placeholder="Caste......."/>
               
               
                 <p className="label1">জাতীয়তা  <b>/</b> Nationality</p>

                 <input type="text" 
                 id="nation" 
                 name="" 
                 placeholder="Nationality (Bangladeshi / Indian ........)"
                 onkeyup="filterFunction()"
                 />


                 <p className="label1">শিক্ষার্থীর মোবাইল নম্বর <b>/</b> Student's mobile number</p>

                 <input type="text" 
                 id="sphone" 
                 name="" 
                 placeholder="Student's Mobile number ........"/>
               
               
               
                 <p className="label1">শিক্ষার্থীর ই - মেইল <b>/</b> Student's Email</p>

                 <input type="text" 
                 id="semail" 
                 name="" 
                 placeholder="Student's Email ........"/>

                 <button class="next_step" onClick={handleNextStep}>
                 Next
                 </button>


               


                </form>

                

              </div>
             </div>
             )}

             {currentStep === 2 && (

              <div className="step-two" id="step-two">
                

              <p className="para1">Permanent Addresses ( স্থায়ী ঠিকানা )</p>

             <div className="form">
               <form>
                 <p className="label">গ্রাম / বাড়ি নম্বর / রোড নম্বর  <b>/</b> (Village/ House number / Road number)</p>
                 <input type="text" 
                 id="village" 
                 name="" 
                 placeholder="Village/ House number / Road number"/>


                 <p className="label1">পোস্ট অফিস <b>/</b> Post office</p>

                 <input type="text" 
                 id="postoffice" 
                 name="" 
                 placeholder="Post office......."/>


                 <p className="label1">পোস্ট কোড <b>/</b> Post code</p>

                 <input type="text" 
                 id="postcode" 
                 name="" 
                 placeholder="Post code......."/>


                 <p className="label1">থানা <b>/</b> Thana</p>

                 <input type="text" 
                 id="thana" 
                 name="" 
                 placeholder="Thana......."/>
                 

                 <p className="label1">জেলা <b>/</b> District</p>

                 <input type="text" 
                 id="district" 
                 name="" 
                 placeholder="District......."/>


                 <p className="label1">দেশ <b>/</b> Country</p>

                 <input type="text" 
                 id="country" 
                 name="" 
                 placeholder="Country......."/>


                 <p className="label1">শিক্ষার্থীর জাতীয় পরিচয়পত্র  / জন্ম সনদ / পাসপোর্ট নম্বর  <b>/</b> (Student's NID / Birth reg. /Passport number)</p>

                 <input type="text" 
                 id="nid" 
                 name="" 
                 placeholder="Student's NID / Birth reg. /Passport number"/>


                 <p className="para1">Present Addresses ( বর্তমান ঠিকানা )</p>

                 <p className="label">বর্তমান ঠিকানা <b>/</b> Present Addresses</p>

                 <textarea type="text" 
                 id="paddress" 
                 name="" 
                 placeholder="present address......."/>



                 <button class="next_step" onClick={handleNextStep}>
                 Next
                 </button>


               


                </form>

                

              </div>

              
             </div>
             
             )}

             {currentStep === 3 && (

              <div className="step-two" id="step-two">
                
              <p className="para1">Educational Qualificaion ( শিক্ষাগত যোগ্যতাসমুহ )</p>

             <div className="form">
               <form>
                 <p className="label">এস.এস.সি বোর্ড<b>/</b> (SSC board)</p>

                 <input type="text" 
                 id="ssc_board" 
                 name="" 
                 placeholder="SSC Board"/>


                 <p className="label1">এস.এস.সি প্রতিষ্ঠান<b>/</b> (SSC Institution)</p>

                 <input type="text" 
                 id="ssc_institution" 
                 name="" 
                 placeholder="SSC Institution"/>


                 <p className="label1">এস.এস.সি পরীক্ষা পাসের সন <b>/</b> (SSC passing year)</p>

                 <input type="text" 
                 id="ssc_year" 
                 name="" 
                 placeholder="SSC passing year"/>


                 <p className="label1">এস.এস.সি পরীক্ষার রোল <b>/</b> (SSC Roll no.)</p>

                 <input type="text" 
                 id="ssc_roll" 
                 name="" 
                 placeholder="SSC Roll no."/>


                 <p className="label1">এস.এস.সি জিপিএ <b>/</b> (SSC GPA)</p>

                 <input type="text" 
                 id="ssc_gpa" 
                 name="" 
                 placeholder="SSC GPA"/>


                 <p className="label1">এস.এস.সি পঠিত বিষয় <b>/</b> (SSC Subject)</p>

                 <input type="text" 
                 id="ssc_sub" 
                 name="" 
                 placeholder="SSC Subject"/>



                 <p className="label1">এইচ.এস.সি বোর্ড<b>/</b> (HSC board)</p>

                 <input type="text" 
                 id="hsc_board" 
                 name="" 
                 placeholder="HSC Board"/>


                 <p className="label1">এইচ.এস.সি প্রতিষ্ঠান<b>/</b> (HSC Institution)</p>

                 <input type="text" 
                 id="hsc_institution" 
                 name="" 
                 placeholder="HSC Institution"/>


                 <p className="label1">এইচ.এস.সি পরীক্ষা পাসের সন <b>/</b> (HSC passing year)</p>

                 <input type="text" 
                 id="hsc_year" 
                 name="" 
                 placeholder="HSC passing year"/>


                 <p className="label1">এইচ.এস.সি পরীক্ষার রোল <b>/</b> (HSC Roll no.)</p>

                 <input type="text" 
                 id="hsc_roll" 
                 name="" 
                 placeholder="HSC Roll no."/>


                 <p className="label1">এইচ.এস.সি জিপিএ <b>/</b> (HSC GPA)</p>

                 <input type="text" 
                 id="hsc_gpa" 
                 name="" 
                 placeholder="HSC GPA"/>


                 <p className="label1">এইচ.এস.সি পঠিত বিষয় <b>/</b> (HSC Subject)</p>

                 <input type="text" 
                 id="hsc_sub" 
                 name="" 
                 placeholder="HSC Subject"/>

                 <button class="next_step" onClick={handleNextStep}>
                 Next
                 </button>


               


                </form>

                

              </div>
             </div>
             
             )}
             
             {currentStep === 4 && (

              <div className="step-two" id="step-two">
                
              <p className="para1">Others ( অন্যান্য )</p>

             <div className="form">
               <form>
                 <p className="label">ভর্তিচ্ছু বিভাগ<b>/</b> (Admitting department)</p>

                 <input type="text" 
                 id="dept_name" 
                 name="" 
                 placeholder="Admitting department"/>


                 <p className="label1">ভর্তিচ্ছু প্রোগ্রাম<b>/</b> (Admitting program)</p>

                 <input type="text" 
                 id="program" 
                 name="" 
                 placeholder="Admitting department"/>


                 <p className="label1">সংযুক্ত হলের নাম<b>/</b> (Hall attached)</p>

                 <input type="text" 
                 id="hall_name" 
                 name="" 
                 placeholder="Hall attached"/>


                 <p className="label1">ছাত্রাবস্থায় কখনো পড়াশোনা বন্ধ থাকলে তার কারণ ( উক্ত কারণ, বহিষ্কার বা বাধ্যতামূলক 
                 শিক্ষা প্রতিষ্ঠান পরিবর্তন অথবা অকৃতকার্য হলে সনদসহ ) উল্লেখ করতে হবে<b>/</b>( Mention causes for break of study, if any (Attach the due documents))</p>

                 <textarea type="text" 
                 id="special_cause_1" 
                 name="" 
                 placeholder="Mention causes for break of study, if any ...."/>



                 <p className="label1">অন্য কোনো অনুষদ, বিভাগ বা শিক্ষা প্রতিষ্ঠানে ভর্তি হয়ে থাকলে তার নাম <b>/</b>( Name of the faculty / Department / institution already admitted )</p>

                 <textarea type="text" 
                 id="special_cause_2" 
                 name="" 
                 placeholder="Name of the faculty / Department / institution already admitted ...."/>


                 <p className="label1">আবেদনকারী সরকারি বা অন্যত্র চাকুরীতে নিযুক্ত রয়েছেন কিনা ( নিযুক্ত থাকলে অফিস প্রধানের নিকট থেকে বিশ্ববিদ্যালয়ে ভর্তির জন্য অনাপত্তিপত্র ) <b>/</b>( Applicants in employment shall submit an NOC issued by the employer )</p>

                 <textarea type="text" 
                 id="special_cause_3" 
                 name="" 
                 placeholder=" Applicants in employment shall submit an NOC issued by the employer ...."/>


                 <p className="label1">স্বীকৃত প্রতিষ্ঠান থেকে প্রাপ্ত বৃত্তি, পদক অথবা পুরষ্কার<b>/</b>( Scholarships, medals, or prizes obtained from any recognized organization )</p>

                 <textarea type="text" 
                 id="special_cause_4" 
                 name="" 
                 placeholder=" Applicants in employment shall submit an NOC issued by the employer ...."/>


                 <button class="next_step" onClick={handleNextStep}>
                 Next
                 </button>

                </form>

                

              </div>
             </div>
             
             )}

             {currentStep === 5 && (

              <div className="step-two" id="step-two">
                
              <p className="para1">Guardian's Information ( অভিভাবকের তথ্য )</p>

             <div className="form">
               <form>
                 <p className="label">অভিভাবকের নাম<b>/</b> Guardian's name</p>

                 <input type="text" 
                 id="gname" 
                 name="" 
                 placeholder="Guardian's name"/>


                 <p className="label1">অভিভাবকের পেশা<b>/</b> Guardian's occupation</p>

                 <input type="text" 
                 id="goccupation" 
                 name="" 
                 placeholder="Guardian's occupation"/>


                 <p className="label1">অভিভাবকের মাসিক আয়<b>/</b> Guardian's monthly income</p>

                 <input type="text" 
                 id="gincome" 
                 name="" 
                 placeholder="Guardian's monthly income"/>


                 <p className="label1">সম্পর্ক<b>/</b> Relation</p>

                 <input type="text" 
                 id="grelation" 
                 name="" 
                 placeholder="Relation like father/ mother /brother...."/>


                 <p className="label1">গ্রাম / বাড়ি নম্বর / রোড নম্বর  <b>/</b> (Village/ House number / Road number)</p>
                 <input type="text" 
                 id="gvillage" 
                 name="" 
                 placeholder="Village/ House number / Road number"/>


                 <p className="label1">পোস্ট অফিস <b>/</b> Post office</p>

                 <input type="text" 
                 id="gpostoffice" 
                 name="" 
                 placeholder="Post office......."/>


                 <p className="label1">পোস্ট কোড <b>/</b> Post code</p>

                 <input type="text" 
                 id="gpostcode" 
                 name="" 
                 placeholder="Post code......."/>


                 <p className="label1">থানা <b>/</b> Thana</p>

                 <input type="text" 
                 id="gthana" 
                 name="" 
                 placeholder="Thana......."/>
                 

                 <p className="label1">জেলা <b>/</b> District</p>

                 <input type="text" 
                 id="gdistrict" 
                 name="" 
                 placeholder="District......."/>


                 <p className="label1">দেশ <b>/</b> Country</p>

                 <input type="text" 
                 id="gcountry" 
                 name="" 
                 placeholder="Country......."/>


                 <p className="label1">অভিভাবকের জাতীয় পরিচয়পত্র  / জন্ম সনদ / পাসপোর্ট নম্বর  <b>/</b> (Guardian's NID / Birth reg. /Passport number)</p>

                 <input type="text" 
                 id="gnid" 
                 name="" 
                 placeholder="Guardian's NID / Birth reg. /Passport number"/>


                 <p className="label1">মোবাইল নম্বর<b>/</b>Gaurdian's Mobile number</p>

                 <input type="text" 
                 id="gphone" 
                 name="" 
                 placeholder="mobile number ......."/>

                </form>

              </div>


              <p className="para3">Legal Guardian's Information in absence of father<br></br> ( পিতার অবর্তমানে আইনানুগ অভিভাবকের তথ্য )</p>

             <div className="form">
               <form>
                 <p className="label">পিতার অবর্তমানে আইনানুগ অভিভাবকের নাম<b>/</b> Legal Guardian's name in absence of father</p>

                 <input type="text" 
                 id="lgname" 
                 name="" 
                 placeholder="Legal Guardian's name in absence of father"/>


                 <p className="label1">পেশা<b>/</b> Occupation</p>

                 <input type="text" 
                 id="lgoccupation" 
                 name="" 
                 placeholder="Occupation"/>


                 <p className="label1">মাসিক আয়<b>/</b> Monthly income</p>

                 <input type="text" 
                 id="lgincome" 
                 name="" 
                 placeholder="Monthly income"/>


                 <p className="label1">সম্পর্ক<b>/</b> Relation</p>

                 <input type="text" 
                 id="lgrelation" 
                 name="" 
                 placeholder="Relation like father/ mother /brother...."/>


                 <p className="label1">গ্রাম / বাড়ি নম্বর / রোড নম্বর  <b>/</b> (Village/ House number / Road number)</p>
                 <input type="text" 
                 id="lgvillage" 
                 name="" 
                 placeholder="Village/ House number / Road number"/>


                 <p className="label1">পোস্ট অফিস <b>/</b> Post office</p>

                 <input type="text" 
                 id="lgpostoffice" 
                 name="" 
                 placeholder="Post office......."/>


                 <p className="label1">পোস্ট কোড <b>/</b> Post code</p>

                 <input type="text" 
                 id="lgpostcode" 
                 name="" 
                 placeholder="Post code......."/>


                 <p className="label1">থানা <b>/</b> Thana</p>

                 <input type="text" 
                 id="lgthana" 
                 name="" 
                 placeholder="Thana......."/>
                 

                 <p className="label1">জেলা <b>/</b> District</p>

                 <input type="text" 
                 id="lgdistrict" 
                 name="" 
                 placeholder="District......."/>


                 <p className="label1">দেশ <b>/</b> Country</p>

                 <input type="text" 
                 id="lgcountry" 
                 name="" 
                 placeholder="Country......."/>


                 <p className="label1">অভিভাবকের জাতীয় পরিচয়পত্র  / জন্ম সনদ / পাসপোর্ট নম্বর  <b>/</b> (Guardian's NID / Birth reg. /Passport number)</p>

                 <input type="text" 
                 id="lgnid" 
                 name="" 
                 placeholder="Guardian's NID / Birth reg. /Passport number"/>


                 <p className="label1">মোবাইল নম্বর<b>/</b>Gaurdian's Mobile number</p>

                 <input type="text" 
                 id="lgphone" 
                 name="" 
                 placeholder="mobile number ......."/>

                </form>

              </div>


              <p className="para3">Local Guardian's Information ( স্থানীয় অভিভাবকের তথ্য )</p>

             <div className="form">
               <form>
                 <p className="label">স্থানীয় অভিভাবকের নাম<b>/</b> Local Guardian's name</p>

                 <input type="text" 
                 id="logname" 
                 name="" 
                 placeholder="Local Guardian's name"/>


                 <p className="label1">সম্পর্ক<b>/</b> Relation</p>

                 <input type="text" 
                 id="logrelation" 
                 name="" 
                 placeholder="Relation like father/ mother /brother...."/>


                 <p className="label1">গ্রাম / বাড়ি নম্বর / রোড নম্বর  <b>/</b> (Village/ House number / Road number)</p>
                 <input type="text" 
                 id="logvillage" 
                 name="" 
                 placeholder="Village/ House number / Road number"/>


                 <p className="label1">পোস্ট অফিস <b>/</b> Post office</p>

                 <input type="text" 
                 id="logpostoffice" 
                 name="" 
                 placeholder="Post office......."/>


                 <p className="label1">পোস্ট কোড <b>/</b> Post code</p>

                 <input type="text" 
                 id="logpostcode" 
                 name="" 
                 placeholder="Post code......."/>


                 <p className="label1">থানা <b>/</b> Thana</p>

                 <input type="text" 
                 id="logthana" 
                 name="" 
                 placeholder="Thana......."/>
                 

                 <p className="label1">জেলা <b>/</b> District</p>

                 <input type="text" 
                 id="logdistrict" 
                 name="" 
                 placeholder="District......."/>


                 <p className="label1">দেশ <b>/</b> Country</p>

                 <input type="text" 
                 id="logcountry" 
                 name="" 
                 placeholder="Country......."/>


                 <p className="label1">অভিভাবকের জাতীয় পরিচয়পত্র  / জন্ম সনদ / পাসপোর্ট নম্বর  <b>/</b> (Guardian's NID / Birth reg. /Passport number)</p>

                 <input type="text" 
                 id="lognid" 
                 name="" 
                 placeholder="Guardian's NID / Birth reg. /Passport number"/>


                 <p className="label1">মোবাইল নম্বর<b>/</b>Gaurdian's Mobile number</p>

                 <input type="text" 
                 id="gphone" 
                 name="" 
                 placeholder="mobile number ......."/>


                 <button class="next_step" onClick={null}>
                 Finish
                 </button>

                </form>

              </div>


             </div>
             
             )}

             

            </div>
          </div>
          
        </div>

      </div>
      
    </div>
  );
};

export default Application;
