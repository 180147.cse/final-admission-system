import React from "react";
import { Routes, Route } from "react-router-dom";
import Application from "./components/applicationpage/Application";
import Home from "./pages/Home/Home";

const App = () => {
  return (
    <div>
      <Routes>
        <Route path="/*" element={<Application />}></Route>
      </Routes>
    </div>
  );
};

export default App;
